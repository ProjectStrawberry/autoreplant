package com.gmail.jd4656.autoreplant;

import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.type.Cocoa;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.Random;

public class EventListeners implements Listener {
    private final Material[] hoes = new Material[]{Material.WOODEN_HOE, Material.STONE_HOE, Material.GOLDEN_HOE,
            Material.IRON_HOE, Material.DIAMOND_HOE, Material.NETHERITE_HOE};
    private final Material[] axes = new Material[]{Material.WOODEN_AXE, Material.STONE_AXE, Material.GOLDEN_AXE,
            Material.IRON_AXE, Material.DIAMOND_AXE, Material.NETHERITE_AXE};
    private final AutoReplant plugin;
    Random random = new Random();

    EventListeners(AutoReplant p) {
        plugin = p;
    }

    @EventHandler
    public void blockBreakEvent(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();

        ItemStack itemInHand = player.getInventory().getItemInMainHand();
        boolean validHoe = false;
        boolean validAxe = false;

        for (Material hoeType : hoes) {
            if (itemInHand.getType() == hoeType) {
                validHoe = true;
                break;
            }
        }

        for (Material axeType : axes) {
            if (itemInHand.getType() == axeType) {
                validAxe = true;
                break;
            }
        }

        if (!validAxe && !validHoe) return;

        boolean adjustedDrops = false;
        Collection<ItemStack> drops = block.getDrops(itemInHand, player);
        for (ItemStack drop : drops) {
            if (validAxe) {
                if (block.getType() == Material.COCOA && drop.getType().equals(Material.COCOA_BEANS)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
            }
            if (validHoe) {
                if (block.getType() == Material.NETHER_WART && drop.getType().equals(Material.NETHER_WART)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
                if (block.getType() == Material.WHEAT && drop.getType().equals(Material.WHEAT_SEEDS)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
                if (block.getType() == Material.BEETROOTS && drop.getType().equals(Material.BEETROOT_SEEDS)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
                if (block.getType() == Material.POTATOES && drop.getType().equals(Material.POTATO)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
                if (block.getType() == Material.CARROTS && drop.getType().equals(Material.CARROT)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
                if (block.getType() == Material.SWEET_BERRY_BUSH && drop.getType().equals(Material.SWEET_BERRIES)) {
                    adjustDrop(drop);
                    adjustedDrops = true;
                    break;
                }
            }
        }

        if (adjustedDrops) {
            event.setDropItems(false);

            Damageable meta = (Damageable) itemInHand.getItemMeta();
            assert meta != null;
            int level = itemInHand.getEnchantmentLevel(Enchantment.DURABILITY);

            if (validHoe && (level > 0 && (random.nextInt(100 + 1) < (100 / (level + 1))) || level < 1)) {
                meta.setDamage(meta.getDamage() + 1);
                itemInHand.setItemMeta((ItemMeta) meta);

                if (meta.getDamage() > itemInHand.getType().getMaxDurability()) {
                    player.playEffect(EntityEffect.BREAK_EQUIPMENT_MAIN_HAND);
                    player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                }
            }

            Location loc = block.getLocation();
            for (ItemStack item : drops) {
                if (item.getType() == Material.AIR) continue;

                assert loc.getWorld() != null;
                loc.getWorld().dropItemNaturally(loc, item);
            }

            Material cropType = block.getType();
            final BlockFace facing = (block.getBlockData() instanceof Cocoa ? ((Cocoa) block.getBlockData()).getFacing() : null);

            new BukkitRunnable() {
                @Override
                public void run() {
                    block.setType(cropType);
                    if (block.getBlockData() instanceof Cocoa && facing != null) {
                        Cocoa blockData = (Cocoa) block.getBlockData();
                        blockData.setAge(0);
                        blockData.setFacing(facing);
                        block.setBlockData(blockData);
                    } else {
                        Ageable blockData = (Ageable) block.getBlockData();
                        blockData.setAge(0);
                        block.setBlockData(blockData);
                    }
                }
            }.runTaskLater(plugin, 1);
        }
    }

    void adjustDrop(ItemStack drop) {
        if (drop.getAmount() > 1) {
            drop.setAmount(drop.getAmount() - 1);
        } else {
            drop.setType(Material.AIR);
        }
    }
}
