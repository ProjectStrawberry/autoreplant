package com.gmail.jd4656.autoreplant;

import org.bukkit.plugin.java.JavaPlugin;

public class AutoReplant extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
    }
}
